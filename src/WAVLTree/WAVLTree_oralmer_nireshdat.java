package WAVLTree; //TODO: remove package

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * WAVLTree
 * <p>
 * An implementation of a WAVL Tree with
 * distinct integer keys and info
 */

public class WAVLTree_oralmer_nireshdat { //TODO: change back to WAVLTree

    private WAVLNode sentinel;
    private WAVLNode ext;

    public WAVLTree_oralmer_nireshdat(){
        this.sentinel = new WAVLNode(null, true);
        this.ext = new WAVLNode(null, false);
    }

    public static void main(String[] args) { //TODO: remove
        Random mijo = new Random();
        for(int i = 1; i<2; i++){
            int num = i*10000000;
            System.out.println("TEST ON " + Integer.toString(num));
            int value;
            int balances = 0;
            int insertbalances = 0;
            int deletebalances = 0;
            int maxinsbal = 0;
            int maxdelbal = 0;
            WAVLTree_oralmer_nireshdat tree = new WAVLTree_oralmer_nireshdat();
            for (int j = 0; j < num; j++){
                value = mijo.nextInt();
                balances = tree.insert(value, Integer.toString(value));
                maxinsbal = Math.max(maxinsbal, balances);
                insertbalances += balances;
            }
            while (!tree.empty()){
                balances = tree.delete(Integer.parseInt(tree.min()));
                maxdelbal = Math.max(maxdelbal, balances);
                deletebalances += balances;
            }
            System.out.println("Insert average balances - " + Float.toString((float)insertbalances/num));
            System.out.println("Max insert rebalance - " + Integer.toString(maxinsbal));
            System.out.println("Delete average balances - " + Float.toString((float) deletebalances/num));
            System.out.println("Max delete rebalance - " + Integer.toString(maxdelbal));
        }
    }

    /**
     * public boolean empty()
     * <p>
     * returns true if and only if the tree is empty
     */
    public boolean empty() {
        return (sentinel.getLeft() == null || sentinel.getLeft() == ext);
    }

    /**
     * public String search(int k)
     * <p>
     * returns the info of an item with key k if it exists in the tree
     * otherwise, returns null
     */
    public String search(int k) {
        if(empty()){
            return null;
        }
        IWAVLNode node = getRoot();
        do {
            if(node.getKey() == k) {
                return node.getValue();
            }
            if(node.getKey() < k){
                node = node.getRight();
            }
            else {
                node = node.getLeft();
            }
        }while(node != null && node != ext);
        return null;
    }

    /**
     * public int insert(int k, String i)
     * <p>
     * inserts an item with key k and info i to the WAVL tree.
     * the tree must remain valid (keep its invariants).
     * returns the number of rebalancing operations, or 0 if no rebalancing operations were necessary.
     * returns -1 if an item with key k already exists in the tree.
     */
    public int insert(int k, String i) {
        WAVLNode node = new WAVLNode(null, ext, ext, k, i);
        WAVLNode head = getRoot();
        if (getRoot() == null){
            sentinel.setLeft(node);
            return 0;
        }
        boolean flag = true;
        while(flag) {
            if (head.getKey() == node.getKey()) {
                return -1;
            } else if (head.getKey() < node.getKey()) {
                if (head.getRight() != ext)
                    head = head.getRight();
                else {
                    flag = false;
                    head.setRight(node);
                }
            } else {
                if (head.getLeft() != ext)
                    head = head.getLeft();
                else {
                    flag = false;
                    head.setLeft(node);
                }
            }
        }
        return node.insertRebalance();
    }
    /**
     * public int delete(int k)
     * <p>
     * deletes an item with key k from the binary tree, if it is there;
     * the tree must remain valid (keep its invariants).
     * returns the number of rebalancing operations, or 0 if no rebalancing operations were needed.
     * returns -1 if an item with key k was not found in the tree.
     */

    public int delete(int k) {
        if(empty()){
            return -1;
        }
        WAVLNode head = getRoot();
        WAVLNode node = head;
        boolean flag = true;
        while(flag){
            if (head.getRank() == -1)
                return -1;
            if (head.getKey() == k) {
                node = head;
                flag = false;
            }
            else if (head.getKey() > k)
                head = head.getLeft();
            else
                head = head.getRight();
        }
        return node.delete();
    }

    /**
     * public String min()
     * <p>
     * Returns the info of the item with the smallest key in the tree,
     * or null if the tree is empty
     */
    public String min() {
        if(empty()){
            return null;
        }
        IWAVLNode pointer = getRoot();
        while(pointer.getLeft() != ext){
            pointer = pointer.getLeft();
        }
        return  pointer.getValue();
    }

    /**
     * public String max()
     * <p>
     * Returns the info of the item with the largest key in the tree,
     * or null if the tree is empty
     */
    public String max() {
        if(empty()){
            return null;
        }
        IWAVLNode pointer = getRoot();
        while(pointer.getRight() != ext){
            pointer = pointer.getRight();
        }
        return  pointer.getValue();
    }

    /**
     * public int[] keysToArray()
     * <p>
     * Returns a sorted array which contains all keys in the tree,
     * or an empty array if the tree is empty.
     */
    public int[] keysToArray() {
        if(empty()){
            return null;
        }
        int[] arr = new int[size()];
        List<Integer> list = ((WAVLNode)getRoot()).keys();
        for (int i = 0; i<arr.length; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    /**
     * public String[] infoToArray()
     * <p>
     * Returns an array which contains all info in the tree,
     * sorted by their respective keys,
     * or an empty array if the tree is empty.
     */
    public String[] infoToArray() {
        if(empty()){
            return null;
        }
        String[] arr = new String[size()];
        List<String> list = getRoot().infos();
        for (int i = 0; i<arr.length; i++) {
            arr[i] = list.get(i);
        }
        return arr;
    }

    /**
     * public int size()
     * <p>
     * Returns the number of nodes in the tree.
     * <p>
     * precondition: none
     * postcondition: none
     */
    public int size() {
        if(empty()){
            return 0;
        }
        return getRoot().getSubtreeSize();
    }

    /**
     * public int getRoot()
     * <p>
     * Returns the root WAVL node, or null if the tree is empty
     * <p>
     * precondition: none
     * postcondition: none
     */
    public WAVLNode getRoot() {
        if (empty()){
            return null;
        }
        return sentinel.getLeft(); //root always stored in sentinel's left child
    }

    /**
     * public int select(int i)
     * <p>
     * Returns the value of the i'th smallest key (return -1 if tree is empty)
     * Example 1: select(1) returns the value of the node with minimal key
     * Example 2: select(size()) returns the value of the node with maximal key
     * Example 3: select(2) returns the value 2nd smallest minimal node, i.e the value of the node minimal node's successor
     * <p>
     * precondition: size() >= i > 0
     * postcondition: none
     */
    public String select(int i) {
        if (empty()){
            return "-1";
        }
        String[] infoArr = infoToArray();
        return infoArr[i-1];
    }

    /**
     * public interface IWAVLNode
     * ! Do not delete or modify this - otherwise all tests will fail !
     */
    public interface IWAVLNode {
        public int getKey(); //returns node's key (for virtuval node return -1)

        public String getValue(); //returns node's value [info] (for virtuval node return null)

        public IWAVLNode getLeft(); //returns left child (if there is no left child return null)

        public IWAVLNode getRight(); //returns right child (if there is no right child return null)

        public boolean isRealNode(); // Returns True if this is a non-virtual WAVL node (i.e not a virtual leaf or a sentinal)

        public int getSubtreeSize(); // Returns the number of real nodes in this node's subtree (Should be implemented in O(1))


    }

    /**
     * public class WAVLNode
     * <p>
     * If you wish to implement classes other than WAVLTree
     * (for example WAVLNode), do it in this file, not in
     * another file.
     * This class can and must be modified.
     * (It must implement IWAVLNode)
     */
    public class WAVLNode implements IWAVLNode {
        private WAVLNode parent;
        private WAVLNode rightChild;
        private WAVLNode leftChild;
        private int rank;
        private String info;
        private boolean isSentinal;
        private int key;
        public boolean isLeftChild;

        /**
         * Null WAVLNode Constructor
         * <p>
         * Creates external leaf with rank -1 and a set parent, or creates a sentinel
         */
        public WAVLNode(WAVLNode parent, boolean isSentinel) {
            if (isSentinel && parent == null){
                this.isSentinal = isSentinel;
                this.leftChild = null;
                this.rightChild = null;
                this.rank = -2;
                this.parent = null;
            }
            else if (isSentinel && parent != null){
                System.out.println("Sentinel has no daddy!");
            }
            else{
                this.parent = parent;
                this.rank = -1;
                this.leftChild = null;
                this.rightChild = null;
            }

        }

        /**
         * WAVLNode Constructor
         * <p>
         * Creates new non-virtual WAVLNode
         */
        public WAVLNode(WAVLNode parent, WAVLNode rightChild, WAVLNode leftChild, int key, String info){
            this.parent = parent;
            this.rightChild = rightChild;
            this.leftChild = leftChild;
            this.rank = Math.max(leftChild.getRank(), rightChild.getRank()) + 1;
            this.key = key;
            this.info = info;
            this.isSentinal = false;
        }

        public int getRank() {
            return rank;
        }

        public int getKey() {
            if (rank == -1 || isSentinal){
                return -1;
            }
            return key;
        }

        public String getValue() {
            if (rank == -1 || isSentinal){
                return null;
            }
            return info;
        }

        public void setParent(WAVLNode parent){
            this.parent = parent;
        }

        public void setLeft(WAVLNode child){
            this.leftChild = child;
            child.setParent(this);
            child.isLeftChild = true; //setLeft determines that child node's "isLeftChild" member should be true
        }

        public void setRight(WAVLNode child){
            this.rightChild = child;
            child.setParent(this);
            child.isLeftChild = false; //setRight determines that child node's "isLeftChild" member should be false
        }

        public WAVLNode getLeft() {
            return leftChild;
        }

        public WAVLNode getRight() {
            return rightChild;
        }

        public WAVLNode getParent() {
            return parent;
        }

        // Returns True if this is a non-virtual WAVL node (i.e not a virtual leaf or a sentinal)
        public boolean isRealNode() {
            return !(isSentinal || rank == -1);
        }

        // recursively calculates and returns size of the subtree of which this node is a root.
        public int getSubtreeSize() {
            if (!isRealNode()){
                return 0;
            }
            int size = 1;
            if (leftChild != null){
                size += leftChild.getSubtreeSize();
            }
            if (rightChild != null){
                size += rightChild.getSubtreeSize();
            }
            return size;
        }

        // returns the successor node of this node using the algorithm taught in class
        public WAVLNode Successor(){
            WAVLNode pointer = this;
            if (this.rightChild.isRealNode()){
                pointer = this.rightChild;
                while(pointer.getLeft().isRealNode()){
                    pointer = pointer.getLeft();
                }
                return pointer;
            }
            while ((!pointer.isLeftChild) && (!pointer.getParent().isSentinal)){
                pointer = pointer.getParent();
            }
            return pointer.parent;
        }

        // returns the predeccessor of this node using the algorithm taught in class
        public WAVLNode Predeccessor(){
            WAVLNode pointer = this;
            if (this.leftChild.isRealNode()){
                pointer = this.leftChild;
                while(pointer.getRight().isRealNode()){
                    pointer = pointer.getRight();
                }
                return pointer;
            }
            while ((pointer.isLeftChild) && (!pointer.getParent().isSentinal)){
                pointer = pointer.getParent();
            }
            return pointer.parent;
        }

        /**
         * KichOutLeaf()
         * <p>
         * takes a leaf out of its tree - sets parent's substitute child to be external.
         * PRE-CONDITION - node MUST be a leaf.
         */
        public void KickOutLeaf(){
            if (isLeftChild)
                parent.setLeft(leftChild);
            else
                parent.setRight(leftChild);
        }

        /**
         * Replace(WAVLNode replacer)
         * <p>
         * replaces this node with a replacer node.
        */
        public void Replace(WAVLNode replacer){
            if(isLeftChild){
                parent.setLeft(replacer);
            }
            else
                parent.setRight(replacer);
            replacer.setLeft(leftChild);
            replacer.setRight(rightChild);
            replacer.rank = rank;
        }

        /**
         * delete()
         * <p>
         * deletes this node from tree.
         * replaces node with successor, or predecessor if none.
         * returns number of rebalancing ops that took place.
         */
        public int delete(){
            if (!(leftChild.isRealNode() || rightChild.isRealNode()) ){//leaf
                KickOutLeaf();
                return parent.deleteRebalance();
            }
            else if(leftChild.isRealNode() && (!rightChild.isRealNode())){ //unary node
                if(isLeftChild){
                    parent.setLeft(leftChild);
                }
                else{
                    parent.setRight(leftChild);
                }
                return parent.deleteRebalance();
            }
            else if(rightChild.isRealNode() && (!leftChild.isRealNode())){ //unary node
                if(isLeftChild){
                    parent.setLeft(rightChild);
                }
                else{
                    parent.setRight(rightChild);
                }
                return parent.deleteRebalance();
            }

            WAVLNode replacer = Successor();
            if(this == replacer){
                replacer = Predeccessor();
            }
            int balances = replacer.delete();
            Replace(replacer);
            return balances;
        }

        /**
         * deleteRebalance()
         * <p>
         * recursively rebalanced the tree starting from this node and going up.
         * uses deletion cases as shown in class
         * returns collective number of balancing ops used.
         */
        private int deleteRebalance() {
                int moves = 0;
                if (isSentinal){
                    return moves;
                }
                if(rank == 1 && IsLeaf()){
                    rank--;
                    return 1 + parent.deleteRebalance();
                }
                if(legal()) {
                    if (parent.isSentinal || parent.legal()) {
                        return moves;
                    }
                    return moves + parent.deleteRebalance();
                }
                switch(deleteCaseCheck()){
                    case 1:
                        rank -= 1;
                        moves += 1;
                        moves += parent.deleteRebalance();
                        break;
                    case 2:
                        rank--;
                        moves += 2;
                        if (rank - leftChild.rank == 2){
                            rightChild.rank--;
                        }
                        else {
                            leftChild.rank--;
                        }
                        moves += parent.deleteRebalance();
                        break;
                    case 3:
                        moves += 1;
                        rank--;
                        if (rank == leftChild.rank){
                            leftChild.rank++;
                            rightRotate();
                        }
                        else{
                            rightChild.rank++;
                            leftRotate();
                        }
                        if(rank == 1 && IsLeaf())
                            rank--;
                            moves += 1;
                        break;
                    case 4:
                        moves += 2;
                        rank -= 2;
                        if (rank - rightChild.rank == -1){
                            rightChild.rank--;
                            rightChild.leftChild.rank += 2;
                            rightChild.rightRotate();
                            leftRotate();
                        }
                        else{
                            leftChild.rank--;
                            leftChild.rightChild.rank += 2;
                            leftChild.leftRotate();
                            rightRotate();
                        }
                        break;
                }
                return moves;
            }

        /**
         * keys()
         * <p>
         * recursively returns sorted list of keys of this node's subtree.
         */
            public List<Integer> keys(){
            List<Integer> keys = new ArrayList<>();
            if(getLeft().isRealNode()){
                keys.addAll(getLeft().keys());
            }
            keys.add(getKey());
            if(getRight().isRealNode()){
                keys.addAll(getRight().keys());
            }
            return keys;
        }

        /**
         * infos()
         * <p>
         * recursively returns info list sorted by their respected keys..
         */
        public List<String> infos(){
            List<String> infos = new ArrayList<>();
            if(getLeft() != null && getLeft().isRealNode()){
                infos.addAll(getLeft().infos());
            }
            infos.add(getValue());
            if(getRight() != null && getRight().isRealNode()){
                infos.addAll(getRight().infos());
            }
            return infos;
        }

        public boolean IsLeaf(){
            return (!leftChild.isRealNode()) && (!rightChild.isRealNode());
        }

        /**
         * insertRebalance()
         * <p>
         * recursively rebalanced the tree starting from this node and going up.
         * uses insert cases as shown in class
         * returns collective number of balancing ops used.
         */
        public int insertRebalance(){
            int moves = 0;
            if (parent.isSentinal){
                return moves;
            }
            if (parent.legal() && legal()){
                return moves;
            }
            switch(insertCaseCheck()){
                case 1:
                    parent.rank += 1;
                    moves += 1;
                    moves += parent.insertRebalance();
                    break;
                case 2:
                    parent.rank--;
                    moves += 1;
                    if (isLeftChild){
                        parent.rightRotate();
                    }
                    else {
                        parent.leftRotate();
                    }
                    break;
                case 3:
                    parent.rank--;
                    rank--;
                    moves += 2;
                    if (isLeftChild){
                        rightChild.rank++;
                        leftRotate();
                        parent.parent.rightRotate();
                    }
                    else{
                        leftChild.rank++;
                        rightRotate();
                        parent.parent.leftRotate();
                    }
                    break;
            }
            return moves;
        }

        /**
         * legal()
         * <p>
         * checks if rank differences of node and children is legal.
         */
        private boolean legal(){
            return ((rank-rightChild.rank == 1 || rank-rightChild.rank == 2) && (rank-leftChild.rank == 1 || rank-leftChild.rank == 2));
        }

        /**
         * insertCaseCheck
         * <p>
         * returns node's insert case number as numbered in class.
         */
        private int insertCaseCheck(){
            int k = rank;
            if (parent.getRank() == k && (parent.getLeft().getRank() == k-1 || parent.getRight().getRank() == k-1)){
                return 1;
            }
            if (isLeftChild){
                if (leftChild.getRank() == k-1){
                    return 2;
                }
                return 3;
            }
            if(rightChild.getRank() == k-1){
                return 2;
            }
            return 3;
        }

        /**
         * deleteCaseCheck
         * <p>
         * returns node's delete case number as numbered in class.
         */
        private int deleteCaseCheck(){
            int k = rank;
            if((leftChild.rank == k-3 && rightChild.rank == k-2) || (rightChild.rank == k-3 && leftChild.rank == k-2)){
                return 1;
            }
            if (leftChild.rank == k-3){
                if(rightChild.rightChild.rank == k-3 && rightChild.leftChild.rank == k-3){
                    return 2;
                }
                if (rightChild.rightChild.rank == k-2){
                    return 3;
                }
                return 4;
            }
            if(!leftChild.isRealNode()){
                System.out.println(Integer.toString(parent.rank));
                System.out.println(Integer.toString(rightChild.rank));
                System.out.println(Integer.toString(leftChild.rank));
                System.out.println(Integer.toString(rank));
            }
            if(leftChild.rightChild.rank == k-3 && leftChild.leftChild.rank == k-3){
                return 2;
            }
            if (leftChild.leftChild.rank == k-2){
                return 3;
            }
            return 4;
        }

        /**
         * private void rightRotate(WAVLNode rotateRoot)
         * <p>
         * Rotates the sub-tree of rotateRoot to the right.
         * precondition: Called when rightRotate is needed and therefore, obviously, utterly possible.
         * postcondition: none
         * </p>
        */
        private void rightRotate(){
            WAVLNode OG_Left = getLeft();
            WAVLNode OG_Parent = getParent();
            if(isLeftChild){
                OG_Parent.setLeft(OG_Left);
            }
            else {
                OG_Parent.setRight(OG_Left);
            }
            setLeft(OG_Left.getRight());
            OG_Left.setRight(this);
        }

        /**
         * private void leftRotate(WAVLNode rotateRoot)
         * <p>
         * Rotates the sub-tree of rotateRoot to the left.
         * precondition: Called when leftRotate is needed and therefore, without doubt, undeniably possible.
         * postcondition: none
         * </p>
         */
        private void leftRotate(){
            WAVLNode OG_Right = getRight();
            WAVLNode OG_Parent = getParent();
            if(isLeftChild){
                OG_Parent.setLeft(OG_Right);
            }
            else {
                OG_Parent.setRight(OG_Right);
            }
            setRight(OG_Right.getLeft());
            OG_Right.setLeft(this);
        }

        public boolean treeLegal(){ //TODO: remove
            if(!isRealNode()){
                return true;
            }
            if (!legal()){
                System.out.println(Integer.toString(rank) + ","+ Integer.toString(rightChild.rank) + "," + Integer.toString(leftChild.rank));
                System.out.println(Integer.toString(key) + ","+ Integer.toString(rightChild.key) + "," + Integer.toString(leftChild.key));
                return false;
            }
            if((leftChild.isRealNode() && leftChild.key > key) || (rightChild.isRealNode() && rightChild.key < key)){
                System.out.println(Integer.toString(key) + "!"+ Integer.toString(rightChild.key) + "!" + Integer.toString(leftChild.key));
                return false;
            }
            if(rank - leftChild.rank == 2 && rank - rightChild.rank == 2 && IsLeaf()){
                System.out.println(Integer.toString(key) + "$" + Integer.toString(rank));
            }
            return (leftChild.treeLegal() && rightChild.treeLegal());
        }
    }

}

