package tester;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * WAVLTree
 *
 * An implementation of a WAVL Tree with
 * distinct integer keys and info
 *
 * An RB_Tree is composed of a sentinel RB_Tree_Node.
 * If the tree is not empty, then the root is the left child
 * of the sentinel
 *
 */

public class WAVLTreeImp {
    WAVLNode sentinel;


    WAVLNode EXT = createEXTNode();

    private WAVLNode createEXTNode() {
        WAVLNode ext = new WAVLNode(); // Used to represent the external leafs
        ext.left = ext;
        ext.right = ext;
        ext.rank = -1;
        ext.parity = 1;
        return ext;
    }

    public WAVLTreeImp() {
        this.sentinel = new WAVLNode(EXT, null, -1, null);
    }

    /**
     * public boolean empty()
     *
     * returns true if and only if the tree is empty
     *
     */
    public boolean empty() {
        return sentinel.left == EXT;
    }

    /**
     * public String search(int k)
     *
     * returns the info of an item with key k if it exists in the tree
     * otherwise, returns null
     */
    public String search(int k)
    {
        return sentinel.left.find(k).info;
    }

    /**
     * public int insert(int k, String i)
     *
     * inserts an item with key k and info i to the WAVL tree.
     * the tree must remain valid (keep its invariants).
     * returns the number of rebalancing operations, or 0 if no rebalancing operations were necessary.
     * returns -1 if an item with key k already exists in the tree.
     */
    public int insert(int k, String i) {
        WAVLNode x = new WAVLNode(EXT,EXT,k,i);
        int rebalanceOperations = 0;
        if (sentinel.left==EXT) {
            // insert into an empty tree
            sentinel.leftChild(x);
        } else {
            // insert into a non-empty tree
            WAVLNode y = sentinel.left.insertPosition(k);

            if (k == y.key) {
                y.info = i;
                rebalanceOperations = -1;
            } else {
                if (k < y.key) {
                    y.leftChild(x);
                } else {
                    y.rightChild(x);
                }
                rebalanceOperations = x.insertRebalance();
            }
        }
        return rebalanceOperations;
    }

    /**
     * public int delete(int k)
     *
     * deletes an item with key k from the binary tree, if it is there;
     * the tree must remain valid (keep its invariants).
     * returns the number of rebalancing operations, or 0 if no rebalancing operations were needed.
     * returns -1 if an item with key k was not found in the tree.
     */
    public int delete(int k)
    {
        WAVLNode toDel = sentinel.find(k);
        if (toDel == null) {
            return -1;
        } else {
            return toDel.delete();
        }
    }

    /**
     * public String min()
     *
     * Returns the ifo of the item with the smallest key in the tree,
     * or null if the tree is empty
     */
    public String min()
    {
        return sentinel.min().info;
    }

    /**
     * public String max()
     *
     * Returns the info of the item with the largest key in the tree,
     * or null if the tree is empty
     */
    public String max()
    {
        return sentinel.max().info;
    }

    /**
     * public int[] keysToArray()
     *
     * Returns a sorted array which contains all keys in the tree,
     * or an empty array if the tree is empty.
     */
    public int[] keysToArray()
    {
        if (empty()) {
            return new int[0];
        } else {
            List<Integer> keys = sentinel.left.keyList();
            int[] keysArr = new int[keys.size()];

            for (int i = 0; i < keysArr.length; i++) {
                keysArr[i] = keys.get(i);
            }

            return keysArr;
        }
    }

    /**
     * public String[] infoToArray()
     *
     * Returns an array which contains all info in the tree,
     * sorted by their respective keys,
     * or an empty array if the tree is empty.
     */
    public String[] infoToArray()
    {
        if (empty()) {
            return new String[0];
        } else {
            List<String> infos = sentinel.left.infoList();
            String[] infoArr = new String[infos.size()];

            for (int i = 0; i < infoArr.length; i++) {
                infoArr[i] = infos.get(i);
            }

            return infoArr;
        }

    }

    /**
     * public int size()
     *
     * Returns the number of nodes in the tree.
     *
     * precondition: none
     * postcondition: none
     */
    public int size()
    {
        if (empty()) {
            return 0;
        } else {
            return sentinel.left.keyList().size();
        }

    }

    /**
     * public class WAVLNode
     *
     * If you wish to implement classes other than WAVLTree
     * (for example WAVLNode), do it in this file, not in
     * another file.
     * This is an example which can be deleted if no such classes are necessary.
     */
    public class WAVLNode {
        WAVLNode parent = null;
        WAVLNode left;
        WAVLNode right;
        int key;
        String info;
        int rank;
        int parity;

        public WAVLNode() {
            this(null, null, -1, null);
        }

        public WAVLNode(WAVLNode left, WAVLNode right, int key, String info) {
            this.left = left;
            this.right = right;
            this.key = key;
            this.info = info;
            this.rank = 0;
            this.parity = 0;
        }

        /**
         * Is x a leaf?
         */
        boolean isLeaf() {
            return (left == EXT) && (right == EXT);
        }

        /**
         * Is x the root (not the sentinel)
         */

        boolean isRoot() {
            return parent.right == null;
        }

        /**
         * Is x a left child?
         */
        boolean isLeftChild() {
            return this == parent.left;
        }

        /**
         * Make y the left child of x
         */
        void leftChild(WAVLNode y) {
            left = y;
            y.parent = this;
        }

        /**
         * Make y the right child of x
         */
        void rightChild(WAVLNode y) {
            right = y;
            y.parent = this;
        }

        /**
         * Make y the (left or right) child of x parent, substituting x
         */
        void plant(WAVLNode y) {
            if (this == parent.left) {
                parent.leftChild(y);
            } else {
                parent.rightChild(y);
            }
        }

        /**
         * replace x by y
         */
        void replace(WAVLNode y) {
            plant(y);
            y.leftChild(left);
            y.rightChild(right);
        }

        /**
         * Left rotate
         */
        void leftRotate() {
            WAVLNode y = right;
            plant(y);
            rightChild(y.left);
            y.leftChild(this);
        }

        /**
         * Right rotate
         */
        void rightRotate() {
            WAVLNode y = left;
            plant(y);
            leftChild(y.right);
            y.rightChild(this);
        }

        /**
         * Rotate up
         */
        void rotateUp() {
            if (isLeftChild()) {
                parent.rightRotate();
            } else {
                parent.leftRotate();
            }
        }

        /**
         * find an item in the subtree of the current node
         */
        WAVLNode find(int itemKey) {
            if (itemKey == key) {
                return this;
            } else if (itemKey < key ) {
                if (left == EXT) {
                    return null;
                } else {
                    return left.find(itemKey);
                }
            } else {
                if (right == EXT) {
                    return null;
                } else {
                    return right.find(itemKey);
                }
            }
        }

        /**
         * Return the minimum element in the subtree of x
         */
        WAVLNode min() {
            if (left == EXT) {
                return this;
            } else {
                return left.min();
            }
        }

        /**
         * Return the minimum element in the subtree of x
         */
        WAVLNode max() {
            if (right == EXT) {
                return this;
            } else {
                return right.min();
            }
        }

        /**
         * find an insert position in the subtree of the current node
         */
        WAVLNode insertPosition(int itemKey) {
            if (itemKey <= key) {
                if (left == EXT) {
                    return this;
                } else {
                    return left.insertPosition(itemKey);
                }
            } else {
                if (right == EXT) {
                    return this;
                } else {
                    return right.insertPosition(itemKey);
                }
            }
        }

        /**
         * Rebalance after insertion.
         * x is either the root, or has a rank difference of 0 or 1.
         * If it has a rank difference of 0, this is the only violation.
         */
        int insertRebalance() {
            int rebalanceOperations = 0;

            WAVLNode z = parent;
            WAVLNode y;
            WAVLNode b;
            // if (not x.is_root()) and (x.rank==z.rank):
            if ((!isRoot()) && (parity == z.parity)) {
                if (this == parent.left) {
                    y = z.right;
                    b = right;
                } else {
                    y = z.left;
                    b = left;
                }

                // if z.rank-y.rank==1:
                if (z.parity != y.parity) {
                    // Promote
                    parent.rank += 1;
                    rebalanceOperations += 1;

                    parent.parity ^= 1;

                    rebalanceOperations += parent.insertRebalance();
                } else {
                    // if x.rank-b.rank==2:
                    if (parity == b.parity) {
                        // Rotate
                        rotateUp();
                        rebalanceOperations += 1;

                        z.rank -= 1;
                        z.parity ^= 1;
                    } else {
                        // Double Rotate
                        b.rotateUp();
                        b.rotateUp();
                        rebalanceOperations += 2;

                        b.rank += 1;
                        rank -= 1;
                        z.rank -= 1;

                        b.parity ^= 1;
                        parity ^= 1;
                        z.parity ^= 1;
                    }
                }
            }
            return rebalanceOperations;
        }

        /**
         * delete x
         */
        int delete() {
            int rebalanceOperations = 0;

            WAVLNode y;
            WAVLNode z;
            if (left != EXT && right != EXT) {
                // if x is binary, exchange it with its successor
                // and delete the successor
                y = left.min();
                rebalanceOperations += y.delete();
                replace(y);
                y.rank = rank;
                y.parity = parity;
            } else {
                z = parent;
                if (left == EXT) {
                    y = right;
                } else { // x.right==EXT:
                    y = left;
                }
                plant(y);
                if (z.isLeaf()) {
                    z.rank -= 1;
                    rebalanceOperations += 1;
                    z.parity ^= 1;
                    rebalanceOperations += z.deleteRebalance();
                } else {
                    rebalanceOperations += y.deleteRebalance();
                }
            }
            return rebalanceOperations;
        }

        /**
         * Rebalance after deletion.
         * x is either the root, or a 2-child or a 3-child of its parent,
         * and this is the only possible violation of the rank rule.
         * x may be EXT, but in this case, its parent pointer is correctly set.
         */
        int deleteRebalance() {
            int rebalanceOperations = 0;
            WAVLNode z = parent;
            WAVLNode y;
            WAVLNode a;
            WAVLNode b;
            int y_diff;
            int a_diff;
            int b_diff;
            // if (not x.is_root()) and (z.rank-x.rank==3):
            if ((!isRoot()) && (z.parity != parity)) {
                if (isLeftChild()) {
                    y = z.right;
                    a = y.left;
                    b = y.right;
                } else {
                    y = z.left;
                    a = y.right;
                    b = y.left;
                }
                // y_diff = z.rank - y.rank
                // a_diff = y.rank - a.rank
                // b_diff = y.rank - b.rank
                y_diff = z.parity ^ y.parity;
                a_diff = y.parity ^ a.parity;
                b_diff = y.parity ^ b.parity;
                // if y_diff==2:
                if (y_diff == 0) {
                    // Demote"
                    z.rank -= 1;
                    rebalanceOperations += 1;
                    z.parity ^= 1;
                    rebalanceOperations += z.deleteRebalance();
                    // elif a_diff==2 and b_diff==2:
                } else if ((a_diff == 0) && (b_diff == 0)) {
                    // Double demote
                    y.rank -= 1;
                    z.rank -= 1;
                    rebalanceOperations += 2;
                    y.parity ^= 1;
                    z.parity ^= 1;
                    rebalanceOperations += z.deleteRebalance();
                } else if (b_diff == 1) {
                    // Rotate
                    y.rotateUp();
                    y.rank += 1;
                    z.rank -= 1;
                    rebalanceOperations += 1;
                    y.parity ^= 1;
                    z.parity ^= 1;
                    if (z.isLeaf()) {
                        z.rank -= 1;
                        z.parity ^= 1;
                    }
                } else {
                    // Double Rotate
                    a.rotateUp();
                    a.rotateUp();
                    rebalanceOperations += 2;
                    a.rank += 2;
                    y.rank -= 1;
                    z.rank -= 2;
                    y.parity ^= 1;
                }
            }
            return rebalanceOperations;
        }

        /**
         * return a list with the items in the subtree of the current node
         */
        List<Integer> keyList() {
            List<Integer> list = new ArrayList<>();
            if (left != EXT) {
                list.addAll(left.keyList());
            }
            list.add(key);
            if (right != EXT) {
                list.addAll(right.keyList());
            }
            return list;
        }

        /**
         * return a list with the items in the subtree of the current node
         */
        List<String> infoList() {
            List<String> list = new ArrayList<>();
            if (left != EXT) {
                list.addAll(left.infoList());
            }
            list.add(info);
            if (right != EXT) {
                list.addAll(right.infoList());
            }
            return list;
        }

        /**
         * Check whether the subtree of the current node is a valid RB_Tree
         */

        boolean check() {
            int diff_left = rank - left.rank;
            if ((diff_left != 1) && (diff_left != 2)) {
                System.out.printf("Left rank difference %d %d %d %b", rank, left.rank, diff_left, isRoot());
                return false;
            }
            int diff_right = rank - left.rank;
            if (diff_right != 1 && diff_right != 2) {
                System.out.printf("Right rank difference %d %d %d %b", rank, right.rank, diff_right, isRoot());
                return false;
            }
            if (isLeaf() && (diff_left != 1 || diff_right != 1)) {
                System.out.printf("Leaf rank problem %d %d", diff_left, diff_right);
                return false;
            }
            if (left != EXT) {
                if (left.info.compareTo(info) > 0) {
                    System.out.println("Left order problem");
                    return false;
                }
                if (!left.check()) {
                    return false;
                }
            }
            if (right != EXT) {
                if (right.info.compareTo(info) < 0) {
                    System.out.println("Right order problem");
                    return false;
                }
                if (!right.check()) {
                    return false;
                }
            }
            return true;
        }
    }
}
  

